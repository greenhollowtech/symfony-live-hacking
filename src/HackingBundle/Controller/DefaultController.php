<?php

namespace HackingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * The default home page.
     */
    public function indexAction(Request $request)
    {
        return $this->render(
            'HackingBundle:default:index.html.twig',
            array()
        );
    }
}
