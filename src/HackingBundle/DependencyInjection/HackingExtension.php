<?php

namespace HackingBundle\DependencyInjection;

use HackingBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Load configurations for the HackingBundle.
 */
class HackingExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // Set each bundle configuration value as a container parameter
        array_walk(
            $config,
            array($this, 'setParameters'),
            array('parentKey' => $this->getAlias(), 'container' => $container)
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'hacking';
    }

    /**
     * Set all the configuration values as parameters, recursively.
     *
     * @param array|string $config
     * @param string $key
     * @param array $params
     */
    public function setParameters($config, $key, $params)
    {
        // Set the parameter name by appending the current key to the parent
        $parameterName = $params['parentKey'] . '.' . $key;

        // Set the container parameter
        $params['container']->setParameter($parameterName, $config);

        // If the config is an array, recursively call this function for each array value
        if (is_array($config)) {
            array_walk(
                $config,
                array($this, 'setParameters'),
                array('parentKey' => $parameterName, 'container' => $params['container'])
            );
        }
    }
}
