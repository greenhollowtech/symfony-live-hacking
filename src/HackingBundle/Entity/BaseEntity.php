<?php

namespace HackingBundle\Entity;

/**
 * Common entity base.
 */
abstract class BaseEntity
{
    /**
     * Get an array of values from constants matching a given pattern.
     *
     * @param string $match The string match.
     * @param boolean $keyed Set to true to key the array by the contant name.
     *
     * @return array
     */
    public static function getConstantValues($match, $keyed = false)
    {
        $self = new \ReflectionClass(get_called_class());
        $constants = $self->getConstants();
        ksort($constants);

        $keys = array_keys($constants);
        $assetKeys = array_filter($keys, function($key) use ($match) {
            return preg_match($match, $key);
        });

        $values = array_intersect_key($constants, array_flip($assetKeys));

        return $keyed ? $values : array_values($values);
    }

    /**
     * Trigger modified date.
     */
    public function pokeModified()
    {
        if (property_exists(get_called_class(), 'modified')) {
            $this->setModified(new \DateTime(null, new \DateTimeZone('UTC')));
        }

        if (property_exists(get_called_class(), 'modifiedInt')) {
            $this->setModifiedInt(time());
        }
    }

    /**
     * Stringify the entity.
     *
     * @return string
     */
    public function toString()
    {
        if (method_exists($this, '__toString')) {
            return (string) $this;
        }

        return '';
    }
}
