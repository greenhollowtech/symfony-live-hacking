<?php

namespace HackingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use HackingBundle\Entity\Group;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * User entity, implements the User Interface for security.
 */
class User extends BaseEntity implements AdvancedUserInterface, \Serializable
{
    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                             CONSTANTS                              ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                             PROPERTIES                             ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * @var \DateTime
     */
    protected $created;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var array
     */
    protected $groupList;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var boolean
     */
    protected $isActive;

    /**
     * @var \DateTime
     */
    protected $modified;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $passwordTemp;

    /**
     * @var \DateTime
     */
    protected $passwordTempExpire;

    /**
     * @var boolean
     */
    protected $passwordUpdate;

    /**
     * @var string
     */
    protected $username;

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                            ASSOCIATIONS                            ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $groups;

    /**
     * @var \RatsalleyBundle\Entity\User
     */
    protected $parent;

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                           CUSTOM METHODS                           ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * The constructor.
     */
    public function __construct()
    {
        $this->created  = new \DateTime(null, new \DateTimeZone('UTC'));
        $this->groupList = array();
        $this->isActive = true;
        $this->modified = new \DateTime(null, new \DateTimeZone('UTC'));
        $this->passwordUpdate = false;

        $this->groups = new ArrayCollection();
    }

    /**
     * String representation.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * Get group list, which is an array of names of this user's groups.
     *
     * @param boolean $force Set to true to force a reset.
     *
     * @return array
     */
    public function getGroupList($force = false): array
    {
        if (!$force && $this->groupList) {
            return $this->groupList;
        }

        $this->groupList = array();

        foreach ($this->getGroups() as $group) {
            $this->groupList[] = $group->getName();
        }

        if (!$this->groupList) {
            $this->groupList[] = Group::NAME_USER;
        }

        return $this->groupList;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        $roles = array('ROLE_USER');

        if ($this->isAdmin()) {
            $roles[] = 'ROLE_ADMIN';
        }

        return $roles;
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        // Passwords are encrypted with bcrypt.
        // Return null here to allow bcrypt to do its thing.
        return null;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonExpired()
    {
        // Currently not expiring accounts, so always return true
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonLocked()
    {
        // Currently not auto-locking any accounts, so always return true
        return true;
    }

    /**
     * Check if this user is admin.
     *
     * @return boolean
     */
    public function isAdmin(): bool
    {
        return $this->isInGroup(Group::NAME_ADMIN);
    }

    /**
     * {@inheritDoc}
     */
    public function isCredentialsNonExpired(): bool
    {
        return !$this->passwordUpdate;
    }

    /**
     * {@inheritDoc}
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * Generic check if user is in any group.
     *
     * @param string|array $group The group name or array of names.
     *
     * @return boolean
     */
    public function isInGroup($group): bool
    {
        if (!$this->groupList) {
            $this->getGroupList();
        }

        return array_intersect((array) $group, $this->groupList) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function pokeModified()
    {
        parent::pokeModified();

        if ($this->email !== strtolower($this->email)) {
            $this->email = strtolower($this->email);
        }
    }

    /**
     * {@see \Serializable::serialize()}
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->isActive,
            $this->username,
            $this->password,
            $this->passwordUpdate,
            $this->groupList,
        ));
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->setModified(new \DateTime(null, new \DateTimeZone('UTC')));

        return $this;
    }

    /**
     * Set password.  Auto-resets passwordTemp and passwordUpdate to empty.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        $this->setPasswordTemp('');
        $this->setPasswordUpdate(0);
        $this->setModified(new \DateTime(null, new \DateTimeZone('UTC')));

        return $this;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        $this->setModified(new \DateTime(null, new \DateTimeZone('UTC')));

        return $this;
    }

    /**
     * {@see \Serializable::unserialize()}
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->isActive,
            $this->username,
            $this->password,
            $this->passwordUpdate,
            $this->groupList,
        ) = unserialize($serialized);
    }

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                         GENERATED METHODS                          ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * Set created
     *
     * @param datetimeutc $created
     *
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return datetimeutc
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set modified
     *
     * @param datetimeutc $modified
     *
     * @return User
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return datetimeutc
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set passwordTemp
     *
     * @param string $passwordTemp
     *
     * @return User
     */
    public function setPasswordTemp($passwordTemp)
    {
        $this->passwordTemp = $passwordTemp;

        return $this;
    }

    /**
     * Get passwordTemp
     *
     * @return string
     */
    public function getPasswordTemp()
    {
        return $this->passwordTemp;
    }

    /**
     * Set passwordTempExpire
     *
     * @param datetimeutc $passwordTempExpire
     *
     * @return User
     */
    public function setPasswordTempExpire($passwordTempExpire)
    {
        $this->passwordTempExpire = $passwordTempExpire;

        return $this;
    }

    /**
     * Get passwordTempExpire
     *
     * @return datetimeutc
     */
    public function getPasswordTempExpire()
    {
        return $this->passwordTempExpire;
    }

    /**
     * Set passwordUpdate
     *
     * @param boolean $passwordUpdate
     *
     * @return User
     */
    public function setPasswordUpdate($passwordUpdate)
    {
        $this->passwordUpdate = $passwordUpdate;

        return $this;
    }

    /**
     * Get passwordUpdate
     *
     * @return boolean
     */
    public function getPasswordUpdate()
    {
        return $this->passwordUpdate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param \HackingBundle\Entity\User $parent
     *
     * @return User
     */
    public function setParent(\HackingBundle\Entity\User $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \HackingBundle\Entity\User
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add group
     *
     * @param \HackingBundle\Entity\Group $group
     *
     * @return User
     */
    public function addGroup(\HackingBundle\Entity\Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \HackingBundle\Entity\Group $group
     */
    public function removeGroup(\HackingBundle\Entity\Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
