<?php

namespace HackingBundle\Entity\Query\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Defines the IF() MySql function.
 */
class IfFunction extends FunctionNode
{
    /**
     * @var \Doctrine\ORM\Query\AST\Node The condition.
     */
    public $condition = null;

    /**
     * @var \Doctrine\ORM\Query\AST\Node The negative case.
     */
    public $ifFalse = null;

    /**
     * @var \Doctrine\ORM\Query\AST\Node The positive case.
     */
    public $ifTrue = null;

    /**
     * {@inheritdoc}
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->condition = $parser->ComparisonExpression();
        $parser->match(Lexer::T_COMMA);
        $this->ifTrue = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->ifFalse = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * {@inheritdoc}
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            'IF(%s, %s, %s)',
            $this->condition->dispatch($sqlWalker),
            $this->ifTrue->dispatch($sqlWalker),
            $this->ifFalse->dispatch($sqlWalker)
        );
    }
}
