<?php

namespace HackingBundle\Entity\Query\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Defines the IFNULL() MySql function.
 */
class IfIsNullFunction extends FunctionNode
{
    /**
     * @var Doctrine\ORM\Query\AST\Node The negative case.
     */
    public $ifNotNull = null;

    /**
     * @var Doctrine\ORM\Query\AST\Node The positive case.
     */
    public $ifNull = null;

    /**
     * {@inheritdoc}
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->ifNotNull = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->ifNull = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * {@inheritdoc}
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            'IFNULL(%s, %s)',
            $this->condition->dispatch($sqlWalker),
            $this->ifNotNull->dispatch($sqlWalker),
            $this->ifNull->dispatch($sqlWalker)
        );
    }
}
