<?php

namespace HackingBundle\Entity\Query;

use Doctrine\ORM\Query\Expr as DoctrineExpr;

/**
 * Extension of the Doctrine Query Expr class.
 */
class Expr extends DoctrineExpr
{
    /**
     * @var string
     */
    const ASC = 'ASC';

    /**
     * @var string
     */
    const DESC = 'DESC';

    /**
     * Creates a CONCAT(...) statement clause for multiple elements (the
     * Doctrine ->concat() method only allows for two elements).
     *
     * @param mixed $x First value to concatenate.
     * @param mixed $y Second value - continue with any additional values.
     *
     * @return string
     */
    public function concatAll($x, $y)
    {
        $args = func_get_args();

        $func = new DoctrineExpr\Func('CONCAT', $args);

        return (string) $func;
    }

    /**
     * Creates a CONCAT(...) statement clause for multiple elements, with the
     * first parameter used as a "join" type separator.  The elements to join
     * can either follow as any number of parameters or as a single array (not
     * both, however).
     *
     * @param string $join The join string to be inserted between all the others.
     * @param mixed $x First value to concatenate, or all values to concatenate.
     * @param mixed $y Second value - continue with any additional values.
     *
     * @return string
     */
    public function concatAllWith($join, $x, $y = null)
    {
        $args = func_get_args();

        // if the second argument is an array, concatenate those values only
        if (is_array($x)) {
            $args = $x;
        }
        // otherwise, exclude the join from the concatenation list
        else {
            array_shift($args);
        }

        // interject each concatenation value with the join literal
        $joinArgs = array();
        $lastArg = count($args) - 1;
        for ($i = 0; $i <= $lastArg; $i++) {
            $joinArgs[] = $args[$i];
            if ($i < $lastArg) {
                $joinArgs[] = $join;
            }
        }

        $func = new DoctrineExpr\Func('CONCAT', $joinArgs);

        return (string) $func;
    }

    /**
     * A simple IF funciton.
     *
     * @param mixed $condition The IF condition.
     * @param mixed $then The true case.
     * @param mixed $else The false case.
     *
     * @return string
     */
    public function if($condition, $then, $else)
    {
        $func = new DoctrineExpr\Func('IF', array($condition, $then, $else));

        return (string) $func;
    }

    /**
     * An IF funciton to return a different value if the test value is NULL.
     *
     * @param mixed $x The initial value.
     * @param mixed $y The value to use if the first is NULL.
     *
     * @return string
     */
    public function ifIsNull($x, $y)
    {
        $func = new DoctrineExpr\Func('IF_IS_NULL', array($x, $y));

        return (string) $func;
    }
}
