<?php

namespace HackingBundle\Entity\Hydration;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;

/**
 * Hydrator for a single column to flatten out the result array.
 */
class SingleColumnHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    const NAME = 'single_column_hydrator';

    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
