<?php

namespace HackingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Group entity, defines groups users can be associated with.
 */
class Group extends BaseEntity
{
    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                             CONSTANTS                              ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * @var string
     */
    const NAME_ADMIN = 'admin';

    /**
     * @var string
     */
    const NAME_USER = 'user';

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                             PROPERTIES                             ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                            ASSOCIATIONS                            ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $users;

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                           CUSTOM METHODS                           ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * The constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
        ╔════════════════════════════════════════════════════════════════════════════════════╗
        ╠═══════¤                         GENERATED METHODS                          ¤═══════╣
        ╚════════════════════════════════════════════════════════════════════════════════════╝
     */

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user
     *
     * @param \HackingBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(\HackingBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \HackingBundle\Entity\User $user
     */
    public function removeUser(\HackingBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
