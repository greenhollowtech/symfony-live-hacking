#!/bin/bash

##########################################
# Logrotate source for the deploy script #
##########################################

DEPLOY_USER=`whoami`

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Replace application path with ""
ABSOLUTE_PATH="${ABSOLUTE_PATH/\/src\/HackingBundle\/Resources\/deploy/}"

LOGROTATE_CONFIG="${ABSOLUTE_PATH}/var/logs/*.log {
    su ${DEPLOY_USER} ${DEPLOY_USER}
    daily
    missingok
    rotate 7
    compress
    delaycompress
    notifempty
    create 660 ${DEPLOY_USER} ${DEPLOY_USER}
    sharedscripts
    postrotate
        ${ABSOLUTE_PATH}/bin/postlogrotate ${ABSOLUTE_PATH}/var/logs/*.log
    endscript
}
"
