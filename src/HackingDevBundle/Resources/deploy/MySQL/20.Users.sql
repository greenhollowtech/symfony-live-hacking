-- Creating the admin and test users
INSERT INTO `Users`
    (`created`, `email`, `isActive`, `modified`, `password`, `passwordUpdate`, `username`)
VALUES
    ('{{ TIMESTAMP }}', '{{ server_admin_email }}', true, '{{ TIMESTAMP }}', 'hacking', false, 'admin'),
    ('{{ TIMESTAMP }}', 'rabbit@symfony-live-hacking.com', true, '{{ TIMESTAMP }}', 'hacking', false, 'rabbit'),
    ('{{ TIMESTAMP }}', 'chicken@symfony-live-hacking.com', true, '{{ TIMESTAMP }}', 'hacking', false, 'chicken')
;
