-- Assigning admin and test users to groups
INSERT INTO `users_groups`
    (`group_id`, `user_id`)
VALUES
    (
        (SELECT `id` FROM `Groups` WHERE `name` LIKE 'admin'),
        (SELECT `id` FROM `Users` WHERE `username` LIKE 'admin')
    )
;
