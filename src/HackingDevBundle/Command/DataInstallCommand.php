<?php
namespace HackingDevBundle\Command;

use Aws\Ec2\Exception\Ec2Exception;
use Gedmo\Uploadable\FileInfo\FileInfoArray;
use HackingBundle\Command\HackingCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Install development data.
 */
class DataInstallCommand extends HackingCommand
{
    /**
     * @var string
     */
    protected $timestamp;

    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('hacking:data:install')
            ->setDescription('Install data in a fresh schema for development work.')
        ;
    }

    /**
     * Execute the command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface Input interface
     * @param \Symfony\Component\Console\Output\OutputInterface Output interface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        // This command should only be run in a dev environment
        if (strpos($this->environment, 'dev') === false) {
            $this->error = "This command can only be run on a development environment!";
            return $this->end();
        }

        // Set the command properties
        $timestamp = new \DateTime(null, new \DateTimeZone('UTC'));
        $this->timestamp = $timestamp->format('Y-m-d H:i:s');

        // Clear the existing database
        $this->returnCode = $this->clearData();
        if ($this->returnCode || $this->error) {
            return $this->end();
        }

        // Install a fresh data set
        $this->returnCode = $this->installData();
        if ($this->returnCode || $this->error) {
            return $this->end();
        }

        // Encrypt new passwords for all test users
        $this->returnCode = $this->updatePasswords();
        if ($this->returnCode || $this->error) {
            return $this->end();
        }

        // End the command process
        $this->end();
    }

    /**
     * Clear any existing data by dropping the database and rebuilding it.
     *
     * @return integer
     */
    protected function clearData(): int
    {
        // Drop the existing default database
        $drop = $this->getApplication()->find('doctrine:database:drop');
        $dropArgs = array(
            'command' => 'doctrine:database:drop',
            '--force' => true,
            '--connection' => 'default',
        );

        $drop->run(new ArrayInput($dropArgs), $this->output);

        // Close the original connection because it lost the reference to the database
        $connection = $this->entityManager->getConnection();
        if ($connection->isConnected()) {
            $connection->close();
        }

        // Recreate the default database
        $create = $this->getApplication()->find('doctrine:database:create');
        $createArgs = array(
            'command' => 'doctrine:database:create',
            '--connection' => 'default',
        );

        $create->run(new ArrayInput($createArgs), $this->output);

        // Rebuild the default schema from the Entity models
        $update = $this->getApplication()->find('doctrine:schema:update');

        $updateArgs = array(
            'command' => 'doctrine:schema:update',
            '--force' => true,
            '--em' => 'default',
        );

        $update->run(new ArrayInput($updateArgs), $this->output);

        return 0;
    }

    /**
     * Install the development data from the deploy MySQL scripts.
     *
     * @return integer
     */
    protected function installData(): int
    {
        $mysqlRoot = __DIR__ . '/../Resources/deploy/MySQL/';
        $mysqlFiles = $this->scanDir($mysqlRoot);

        // Run each MySQL source file in the order configured
        $this->output->writeln('Running queries in <info>' . $mysqlRoot . '</info>');
        foreach ($mysqlFiles as $mysqlFile) {

            // Skip if not a SQL file
            if (!preg_match('/\.sql$/', $mysqlFile)) {
                continue 1;
            }

            $mysqlQueries = explode(';', file_get_contents($mysqlRoot . $mysqlFile));

            // Parse each query
            foreach ($mysqlQueries as $query) {
                $queryString = '';
                $queryLines = explode("\n", $query);
                foreach ($queryLines as $line) {
                    $line = trim($line);
                    if (!$line) {
                        continue 1;
                    }
                    if (preg_match('/^--/', $line)) {
                        if ($this->isVerbose) {
                            $this->output->writeln('<comment>' . $line . '</comment>');
                        }
                        continue 1;
                    }
                    $queryString .= ' ' . $line;
                }

                if (!$queryString) {
                    continue 1;
                }

                // Prepare the statement with our own stuff first
                $queryString = $this->prepare($queryString);

                // Run the MySQL statement
                try {
                    $statement = $this->entityManager->getConnection()->prepare($queryString);
                    $statement->execute();
                }
                catch (DriverException $e) {
                    $this->error = $e->getMessage();
                    return 1;
                }
            }
        }

        return 0;
    }

    /**
     * Prepare a sql string for execution.
     *
     * @param string $sql The MySQL statement.
     *
     * @return string
     */
    protected function prepare(string $sql): string
    {
        // Clean up the query string
        $sql = str_replace('( ', '(', $sql);
        $sql = str_replace(' )', ')', $sql);

        // Replace any known variables
        $sql = preg_replace(
            '/\{\{[ ]*timestamp[ ]*\}\}/i',
            $this->timestamp,
            $sql
        );

        // Replace any remaining with parameter matches or user input
        $helper = $this->getHelper('question');
        $matches = array();
        if (preg_match_all('/\{\{[ ]*([^\}]*[^ ])[ ]*\}\}/', $sql, $matches)) {
            foreach ($matches[1] as $parameter) {
                try {
                    $value = $this->getContainer()->getParameter($parameter);
                }
                catch (InvalidArgumentException $e) {
                    $question = new Question(sprintf('Data for <comment>%s</comment>: ', $parameter));
                    $value = $helper->ask($this->input, $this->output, $question);
                }
                $sql = preg_replace('/\{\{[ ]*' . $parameter . '[ ]*\}\}/', $value, $sql);
            }
        }

        return $sql;
    }

    /**
     * Update the password for all Users to be encrypted.
     *
     * @return integer
     */
    protected function updatePasswords(): int
    {
        $encoder = $this->getContainer()->get('security.password_encoder');

        // Get all the test Users
        $users = $this->entityManager->getRepository('HackingBundle:User')->findAll();

        // Encrypt and set the password for each
        $this->output->writeln('Encrypting passwords for ' . count($users) . ' Users.');
        foreach ($users as $user) {
            if ($this->isVerbose) {
                $this->output->writeln('-- <comment>' . $user->getUsername() . '</comment>');
            }
            $encodedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPassword);
        }
        $this->entityManager->flush();

        return 0;
    }
}