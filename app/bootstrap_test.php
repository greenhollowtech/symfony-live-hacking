<?php

/**
 * Executed after the regular autoload process, but prior to beginning
 * processing with PHPUnit.
 */

require_once __DIR__ . '/AppKernel.php';

// re-enable garbage collection, which the Symfony PHPUnit Bridge disables
gc_enable();

// boot the framework kernel in the "test" environment
$kernel = new AppKernel('test', true);
$kernel->boot();

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\CreateSchemaDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DropSchemaDoctrineCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

// initialize the console application with the necessary commands
$application = new Application($kernel);
$application->add(new CreateSchemaDoctrineCommand());
$application->add(new DropSchemaDoctrineCommand());

$output = new ConsoleOutput();

$container = $application->getKernel()->getContainer();
$connection = $container->get('doctrine.dbal.default_connection');

$output->writeln('<fg=red;options=bold>Bootstrapping application prior to executing tests:</fg=red;options=bold>');
$output->writeln('<fg=red;options=bold>MySQL Database : ' . $connection->getDatabase() . ' on ' . $connection->getHost()  . '</fg=red;options=bold>');

// drop the current database schema
$application->find('doctrine:schema:drop')->run(
    new ArrayInput(array(
      'command' => 'doctrine:schema:drop',
      '--force' => true,
    )),
    $output
);

// create the database schema from the mappings
$application->find('doctrine:schema:create')->run(
    new ArrayInput(array(
        'command' => 'doctrine:schema:create',
    )),
    $output
);

$output->writeln('<info>Executing PHPUnit...</info>');
