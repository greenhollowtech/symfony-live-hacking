<VirtualHost *:80>
	ServerName symfony-live-hacking.com
	ServerAlias www.symfony-live-hacking.com

	ServerAdmin israel@firebrandtech.com
	DocumentRoot /var/www/symfony-live-hacking.com/current/web

	ErrorLog ${APACHE_LOG_DIR}/symfony-live-hacking-error.log
	CustomLog ${APACHE_LOG_DIR}/symfony-live-hacking-access.log combined

	SetEnv SYMFONY_LIVE_HACKING_ENVIRONMENT dev

	<Directory "/var/www/symfony-live-hacking.com/current/web">
		AllowOverride All
	</Directory>
</VirtualHost>

