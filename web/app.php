<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

// Get the environment variable from the Apache virtual host configuration
$environment = empty($_SERVER['SYMFONY_LIVE_HACKING_ENVIRONMENT']) ? 'prod' : $_SERVER['SYMFONY_LIVE_HACKING_ENVIRONMENT'];

require __DIR__ . '/../app/autoload.php';
if ($environment === 'prod' && PHP_VERSION_ID < 70000) {
    include_once __DIR__ . '/../var/bootstrap.php.cache';
}

$debug = false;
if (in_array($environment, array('dev', 'test'))) {
    $debug = true;
    Debug::enable();
}

$kernel = new AppKernel($environment, $debug);
if (PHP_VERSION_ID < 70000) {
    $kernel->loadClassCache();
}

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
