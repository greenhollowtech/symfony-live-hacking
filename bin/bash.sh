BLUE="\e[34m"
GREEN="\e[32m"
NORMAL="\e[0m"
ORANGE="\e[38;5;202m"
PURPLE="\e[35m"
RED="\e[31m"
YELLOW="\e[33m"

ENVIRONMENTS=(dev prod)

# Run migrations for a configuration
function runMigrations()
{
    local CONFIG="$1"
    local MANAGER="$2"
    local STATUS="$(bin/console doctrine:migrations:status --env=${ENVIRONMENT} --configuration=${CONFIG} --em=${MANAGER})"

    case "${STATUS}" in
        *"Already at latest version"*)
            # Do nothing
            ;;
        *)
            runNextMigration $CONFIG $MANAGER
            ;;
    esac
}

# Run the next migration for a configuration
function runNextMigration()
{
    local CONFIG="$1"
    local MANAGER="$2"
    bin/console doctrine:migrations:migrate next --env=$ENVIRONMENT --configuration=$CONFIG --em=$MANAGER

    runMigrations $CONFIG $MANAGER
}

# Set the ACL for a file or directory
function setAccessControlList()
{
    local FILEPATH="$1"
    local USESUDO="$2"
    local SILENT="$3"

    if [ "${USESUDO}" ]
    then
        local CHECKACL="$(sudo getfacl --access ${FILEPATH})"
    else
        local CHECKACL="$(getfacl --access ${FILEPATH})"
    fi

    if [ -f "${FILEPATH}" ]
    then
        local PERMISSION="rw-"
    else
        local PERMISSION="rwx"
    fi

    case "${CHECKACL}" in
        *user:www-data:${PERMISSION}*mask::rw*)
            if [ ! "${SILENT}" ]
            then
                printf "${BLUE}ACL validated for ${ORANGE}${FILEPATH}${BLUE}.${NORMAL}\n"
            fi
            ;;
        *)
            if [ ! "${SILENT}" ]
            then
                printf "${BLUE}Setting ACL for ${ORANGE}${FILEPATH}${BLUE}...${NORMAL}\n"
            fi
            if [ "${USESUDO}" ]
            then
                sudo setfacl -R --mask -m u:www-data:${PERMISSION} -m u:`whoami`:${PERMISSION} ${FILEPATH}
                sudo setfacl -dR --mask -m u:www-data:${PERMISSION} -m u:`whoami`:${PERMISSION} ${FILEPATH}
            else
                setfacl -R --mask -m u:www-data:${PERMISSION} -m u:`whoami`:${PERMISSION} ${FILEPATH}
                setfacl -dR --mask -m u:www-data:${PERMISSION} -m u:`whoami`:${PERMISSION} ${FILEPATH}
            fi
            ;;
    esac
}

# validate the deploy environment
function validateEnvironment()
{
    local ENVIRONMENT="$1"

    if [ "${ENVIRONMENT}" == '' ]
    then
        return 0
    fi

    for VALID in ${ENVIRONMENTS[*]}
    do
        if [ $ENVIRONMENT == $VALID ]
        then
            return 1
        fi
    done

    return 0
}
