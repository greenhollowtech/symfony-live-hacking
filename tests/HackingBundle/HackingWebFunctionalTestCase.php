<?php

namespace Tests\HackingBundle;

use HackingBundle\Entity\User;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Tests\HackingBundle\HackingFunctionalTestCase;

/**
 * Set up the properties and methods common to all controller tests.
 */
class HackingWebFunctionalTestCase extends HackingFunctionalTestCase
{
    /**
     * @var \appWeb_testDebugProjectContainer
     */
    protected $container;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    protected $client;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->client = self::createClient(array('environment' => getenv('SYMFONY_LIVE_HACKING_ENVIRONMENT')));
        $this->container = $this->client->getContainer();

        parent::setUp();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();

        // Unset properties
        unset($this->client);
    }

    /**
     * Assert a redirect response has been returned.
     *
     * @param string $redirectPath The redirect target.
     */
    protected function assertRedirect(string $redirectPath)
    {
        $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $headers = $this->client->getResponse()->headers->all();
        $this->assertSame($redirectPath, $headers['location'][0]);
    }

    /**
     * Assert a redirect response to the login page has been returned.
     */
    protected function assertRedirectToLogin()
    {
        $this->assertRedirect('http://localhost/login');
    }

    /**
     * Assert a page was forbidden to the logged in user.
     */
    protected function assertResponseForbidden()
    {
        $this->assertSame(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Assert a page was successfully returned in the response.
     */
    protected function assertResponseOk()
    {
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Log the user in.
     *
     * @param \HackingBundle\Entity\User
     */
    protected function logInUser(User $user)
    {
        // Create a user token
        $roles = array('ROLE_USER');
        if ($user->isAdmin()) {
            $roles[] = 'ROLE_ADMIN';
        }
        $token = new UsernamePasswordToken($user->getUsername(), $user->getPassword(), 'site', $roles);
        $token->setUser($user);

        // Grab the session
        $session = $this->client->getContainer()->get('session');
        $session->set('_security_site', serialize($token));
        $session->save();

        // Try setting a cookie
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    /**
     * Log the user out.
     */
    protected function logOut()
    {
        $this->client->getContainer()->get('security.token_storage')->setToken(NULL);
    }
}
