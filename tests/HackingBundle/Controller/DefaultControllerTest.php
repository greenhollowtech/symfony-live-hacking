<?php

namespace Tests\HackingBundle\Controller;

use Tests\HackingBundle\HackingWebFunctionalTestCase;

class DefaultControllerTest extends HackingWebFunctionalTestCase
{
    public function testIndex()
    {
        $this->client->request('GET', '/');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        // $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
