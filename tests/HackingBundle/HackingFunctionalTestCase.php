<?php

namespace Tests\HackingBundle;

use Doctrine\ORM\EntityManager;
use HackingBundle\Entity\Group;
use HackingBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Set up the properties and methods common to all functional tests.
 */
class HackingFunctionalTestCase extends WebTestCase
{
    /**
     * @var \appTestDebugProjectContainer
     */
    protected $container;

    /**
     * @var array
     */
    protected $groups;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Manager
     */
    protected $entityManager;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        // Initialize the container and entity manager if an extension of this
        // class hasn't yet already
        if (!($this->container instanceof \appTestDebugProjectContainer)
            && !($this->container instanceof \appWeb_testDebugProjectContainer)
        ) {
            self::bootKernel();
            $this->container = static::$kernel->getContainer();
        }
        if (!($this->entityManager instanceof EntityManager)) {
            $this->entityManager = $this->container->get('doctrine')->getManager();
        }

        // Clean out any entities created
        $this->entityManager->createQuery('DELETE FROM HackingBundle:Group')->execute();
        $this->entityManager->createQuery('DELETE FROM HackingBundle:User u WHERE u.parent IS NOT NULL')->execute();
        $this->entityManager->createQuery('DELETE FROM HackingBundle:User')->execute();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        // Unset properties
        unset($this->entityManager);
        unset($this->container);
    }

    /**
     * Create a Group if it doesn't already exist.
     *
     * @param string $name The group name.
     *
     * @return \HackingBundle\Entity\Group
     */
    protected function initGroup($name): Group
    {
        // If a group already exists by the given name, we're done
        if (isset($this->groups[$name]) && $this->groups[$name] instanceof Group) {
            return $this->groups[$name];
        }

        // Create the Group
        $group = new Group();
        $group->setName($name);
        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $this->groups[$name] = $group;
    }

    /**
     * Create the browsing User.
     *
     * @param string $username The unique username in case of multiple users.
     * @param boolean $isAdmin Set to true to create admin User.
     *
     * @return \HackingBundle\Entity\User
     */
    protected function initUser($username = '', $isAdmin = false): User
    {
        $username = $username ? $username : 'testuser';

        // Create the User
        $user = new User();
        $user->setUsername($username);
        $user->setPassword('pa$$word');
        $user->setEmail($username . '@email.com');
        $this->entityManager->persist($user);

        $this->entityManager->flush();

        // If not admin, we're done
        if (!$isAdmin) {
            return $user;
        }

        // Create the admin group if necessary
        $group = $this->initGroup('admin');

        // Add the User to the admin group
        $user->addGroup($group);
        $this->entityManager->persist($user);

        $this->entityManager->flush();

        return $user;
    }
}
