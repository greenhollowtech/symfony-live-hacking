<?php

namespace Tests\HackingBundle;

use HackingBundle\Entity\Group;
use HackingBundle\Entity\User;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Set up the properties and methods common to all unit tests.
 */
class HackingUnitTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
    }

    /**
     * Configure a Doctrine event arguments set.
     *
     * @return \Doctrine\Common\EventArgs
     */
    protected function configureDoctrineEventArgs()
    {
        $eventArgs = $this->getMockBuilder('\Doctrine\Common\EventArgs')
            ->disableOriginalConstructor()
            ->setMethods(array('getEntityManager'))
            ->getMock()
        ;

        return $eventArgs;
    }

    /**
     * Mock the entity manager dependency.
     *
     * @param array $repositories Repositories to get, keyed by get value.
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function configureEntityManager(array $repositories = array())
    {
        // Mock the manager
        $manager = $this->getMockBuilder('\Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->setMethods(array('flush', 'getRepository', 'getUnitOfWork', 'persist', 'refresh', 'remove'))
            ->getMock()
        ;

        $manager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnCallback(function($repository) use ($repositories) {
                return (isset($repositories[$repository]) ? $repositories[$repository] : null);
            }))
        ;

        return $manager;
    }

    /**
     * Mock the Symfony event dispatcher.
     *
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected function configureEventDispatcher()
    {
        // Mock the Symfony event dispatcher
        $dispatcher = $this->getMockBuilder('\Symfony\Component\EventDispatcher\EventDispatcher')
            ->disableOriginalConstructor()
            ->setMethods(array('dispatch'))
            ->getMock()
        ;

        return $dispatcher;
    }

    /**
     * Mock the Logger dependency.
     *
     * @return \Monolog\Logger
     */
    protected function configureLogger()
    {
        // Mock the logger
        $logger = $this->getMockBuilder('\Monolog\Logger')
            ->disableOriginalConstructor()
            ->setMethods(array('error', 'info'))
            ->getMock()
        ;

        return $logger;
    }

    /**
     * Mock a Repository.
     *
     * @param array $methods Additional methods to set as mocked.
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function configureRepository($methods = array())
    {
        $methods = array_unique(array_merge($methods, array('find', 'findBy', 'findOneBy')));

        $repository = $this->getMockBuilder('\Doctrine\ORM\EntityRepository')
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock()
        ;

        return $repository;
    }

    /**
     * Mock the Request dependency.  Parameters, if given, will be set as
     * either the request or query property, depending on the Request type.
     *
     * @param array $parameters The Request parameters.
     * @param string $type The Request type, POST or GET.
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    protected function configureRequest($parameters = array(), $type = 'GET')
    {
        $type = strtoupper($type);

        // Mock the request
        $request = $this->getMockBuilder('\Symfony\Component\HttpFoundation\Request')
            ->disableOriginalConstructor()
            ->setMethods(array('getUri'))
            ->getMock()
        ;

        // Just return a mock URL.
        $request->method('getUri')
            ->will($this->returnValue('/test/request/route'))
        ;

        // If there aren't any parameters, we're done
        if (!$parameters) {
            return $request;
        }

        // Set the request's parameters
        if ($type === 'POST') {
            $request->initialize(array(), $parameters);
        }
        else {
            $request->initialize($parameters);
        }

        return $request;
    }

    /**
     * Mock the RequestStack dependency.  Accepts a mocked Request.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The mocked Request.
     *
     * @return \Symfony\Component\HttpFoundation\RequestStack
     */
    protected function configureRequestStack(Request $request = null)
    {
        // Mock the RequestStack
        $requestStack = $this->getMockBuilder('\Symfony\Component\HttpFoundation\RequestStack')
            ->disableOriginalConstructor()
            ->setMethods(array('getCurrentRequest'))
            ->getMock()
        ;

        // Return the mocked Request
        $requestStack->method('getCurrentRequest')
            ->will($this->returnValue($request ? $request : $this->configureRequest()))
        ;

        return $requestStack;
    }

    /**
     * Mock the Serializer dependency.
     *
     * @return \JMS\Serializer\SerializerBuilder
     */
    protected function configureSerializer()
    {
        // Mock the serializer
        $serializer = $this->getMockBuilder('\JMS\Serializer\Serializer')
            ->disableOriginalConstructor()
            ->setMethods(array('serialize'))
            ->getMock()
        ;

        // Just return a generic response of the type being serialized.
        $serializer->method('serialize')
            ->will($this->returnCallback(
                function ($data) {
                    if (is_object($data)) {
                        return '{serialized: object}';
                    }
                    elseif (is_array($data)) {
                        return '{serialized: array}';
                    }
                    return '{serialized: data}';
                }
            ))
        ;

        return $serializer;
    }

    /**
     * Mock the Unit of Work.
     *
     * @return \Doctrine\ORM\UnitOfWork
     */
    protected function configureUnitOfWork()
    {
        $unitOfWork = $this->getMockBuilder('\Doctrine\ORM\UnitOfWork')
            ->disableOriginalConstructor()
            ->setMethods(array('getCommitOrderCalculator', 'getEntityChangeSet', 'getIdentityMap'))
            ->getMock()
        ;

        return $unitOfWork;
    }

    /**
     * Set up a User of a given group with a given gate key if any.
     *
     * @param string $username A unique username in case of multiple users.
     * @param string $groupName The name of the Group to which the User belongs.
     *
     * @return \Ratsalley\Entity\User
     */
    protected function configureUser($username = null, $groupName = null)
    {
        $username = $username ? $username : 'testuser';

        $user = new User();
        $user->setUsername($username);
        $user->setPassword('pa$$word');
        $user->setEmail($username . '@email.com');

        if ($groupName) {
            $group = new Group();
            $group->setName($groupName);

            $user->addGroup($group);
        }

        return $user;
    }

    /**
     * Get a private or protected method that can then be invoked, for example:
     *    $myClass = new MyClass();
     *    $myMethod = $this->getMethod($myClass, 'myMethod');
     *    $result = $myMethod->invokeArgs($myClass, array($parameterOne, $parameterTwo));
     *
     * @param  string $object The object.
     * @param  string $method The method name.
     *
     * @return  \ReflectionMethod
     */
    protected function getMethod($object, $method)
    {
        $reflector = new \ReflectionClass(get_class($object));
        $method = $reflector->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * Get a private or protected property that can then be accessed, for example:
     *    $myClass = new myClass();
     *    $myProperty = $this->getProperty($myClass, 'myProperty');
     *    $propertyValue = $myProperty->getValue($myClass);
     *    $myProperty->setValue($myClass, $newValue);
     *
     * @param string $object
     * @param string $property
     *
     * @return \ReflectionProperty
     */
    protected function getProperty($object, $property)
    {
        $reflector = new \ReflectionClass(get_class($object));
        $property = $reflector->getProperty($property);
        $property->setAccessible(true);

        return $property;
    }
}
