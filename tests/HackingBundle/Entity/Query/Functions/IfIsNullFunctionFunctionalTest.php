<?php

namespace Tests\HackingBundle\Entity\Query\Functions;

use Tests\HackingBundle\HackingFunctionalTestCase;

/**
 * Verify that the IfIsNullFunction behaves as expected.
 */
class IfIsNullFunctionFunctionalTest extends HackingFunctionalTestCase
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Verify an IF NULL function can be parsed into DQL.
     */
    function testIfIsNullFunction()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('IF_IS_NULL(:one, :two)');

        $this->assertEquals('SELECT IF_IS_NULL(:one, :two)', $queryBuilder->getDql());
    }
}
