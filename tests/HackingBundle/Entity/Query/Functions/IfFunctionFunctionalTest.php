<?php

namespace Tests\HackingBundle\Entity\Query\Functions;

use Tests\HackingBundle\HackingFunctionalTestCase;

/**
 * Verify that the IfFunction behaves as expected.
 */
class IfFunctionFunctionalTest extends HackingFunctionalTestCase
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Verify an IF function can be parsed into DQL.
     */
    function testIfFunction()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('IF(true, :one, :two)');

        $this->assertEquals('SELECT IF(true, :one, :two)', $queryBuilder->getDql());
    }
}
