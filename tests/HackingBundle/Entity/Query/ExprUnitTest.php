<?php

namespace Tests\HackingBundle\Entity\Query;

use HackingBundle\Entity\Query\Expr;
use Tests\HackingBundle\HackingUnitTestCase;

/**
 * Verify that the Query Expr behaves as expected.
 */
class ExprUnitTest extends HackingUnitTestCase
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Verify that a concat clause can be expressed.
     */
    public function testConcatAll()
    {
        $expr = new Expr();
        $this->assertEquals('CONCAT(:x, :y, :z)', $expr->concatAll(':x', ':y', ':z'));
    }

    /**
     * Verify that a concat clause can be expressed with a custom join.
     */
    public function testConcatAllWith()
    {
        $expr = new Expr();
        $this->assertEquals('CONCAT(:x, :join, :y, :join, :z)', $expr->concatAllWith(':join', ':x', ':y', ':z'));
    }

    /**
     * Verify that a simple if clause can be expressed.
     */
    public function testIf()
    {
        $expr = new Expr();
        $this->assertEquals('IF(:boolean, :then, :else)', $expr->if(':boolean', ':then', ':else'));
    }

    /**
     * Verify that a simple if-null clause can be expressed.
     */
    public function testIfIsNull()
    {
        $expr = new Expr();
        $this->assertEquals('IF_IS_NULL(:one, :two)', $expr->ifIsNull(':one', ':two'));
    }
}
