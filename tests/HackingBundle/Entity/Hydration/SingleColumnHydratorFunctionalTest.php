<?php

namespace Tests\HackingBundle\Entity\Hydration;

use Doctrine\DBAL\Driver\PDOStatement;
use HackingBundle\Entity\Hydration\SingleColumnHydrator;
use Tests\HackingBundle\HackingFunctionalTestCase;

/**
 * Verify that the single column hydrator hydrates as expected.
 */
class SingleColumnHydratorFunctionalTest extends HackingFunctionalTestCase
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Verify that we get the flattened array as expected.
     */
    public function testHydrateAllData()
    {
        // Set up some User names as test data
        $userNames = array('one', 'two', 'three');
        foreach ($userNames as $userName) {
            $this->initUser($userName);
        }

        // Set the hydration mode
        $this->entityManager->getConfiguration()
            ->addCustomHydrationMode(SingleColumnHydrator::NAME, SingleColumnHydrator::class)
        ;

        // Build the query
        $query = $this->entityManager->createQueryBuilder()
            ->select('u.username')
            ->from('HackingBundle:User', 'u')
            ->orderBy('u.username', 'ASC')
            ->getQuery()
        ;

        // Verify that we get an array of scalar values
        $this->assertSame(array('one', 'three', 'two'), $query->getResult(SingleColumnHydrator::NAME));
    }
}
